package com.usc.EcommerceBack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EntityScan("beans")
@ComponentScan("handler")
@EnableJpaRepositories("dao")
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class EcommerceBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceBackApplication.class, args);
		//System.out.println("http://localhost:8080/api/products");
		
	}

}
