package beans;

import javax.persistence.*;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name="role")
public class Role implements GrantedAuthority{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;
	
	@Column
	private String type;

	public Role(int id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Role(int id) {
		super();
		this.id = id;
	}
	
	@Override
	public String getAuthority() {
		return type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	


}
