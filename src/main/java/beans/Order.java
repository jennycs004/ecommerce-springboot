package beans;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="orders")
public class Order {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "order_tracking_number")
	private String orderTrackingNumber;
	
	@Column(name="total_price")
	private BigDecimal totalPrice;
	
	@Column(name = "total_quantity")
	private int totalQuantity;	
	
	@Column(name="status")
	private String status;
	
	@Column(name="date_created")
	@CreationTimestamp
	private Date dateCreated;
	
	@Column(name="last_updated")
	@UpdateTimestamp
	private Date lastUpdated;	
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "shipping_address_id", referencedColumnName="id")
	private Address shippingAddress;
	
	
	@ManyToOne
	@JoinColumn(name = "customer_id", referencedColumnName="id")
	@JsonIgnore
	private Customer customer;
	
	@OneToMany(mappedBy="order", cascade= CascadeType.ALL)
	private Set<OrderItem> orderItems = new HashSet<>(); 
	
	public void add(OrderItem orderItem) {
		
		if (orderItem != null) {
			if (orderItems == null) {
				orderItems = new HashSet<>();			
			}
			this.orderItems.add(orderItem);
			orderItem.setOrder(this);
		}
		
	}

	public Order(Long id, String orderTrackingNumber, BigDecimal totalPrice, int totalQuantity, String status,
			Date dateCreated, Date lastUpdated, Address shippingAddress, Customer customer, Set<OrderItem> orderItems) {
		super();
		this.id = id;
		this.orderTrackingNumber = orderTrackingNumber;
		this.totalPrice = totalPrice;
		this.totalQuantity = totalQuantity;
		this.status = status;
		this.dateCreated = dateCreated;
		this.lastUpdated = lastUpdated;
		this.shippingAddress = shippingAddress;
		this.customer = customer;
		this.orderItems = orderItems;
	}

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderTrackingNumber() {
		return orderTrackingNumber;
	}

	public void setOrderTrackingNumber(String orderTrackingNumber) {
		this.orderTrackingNumber = orderTrackingNumber;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Address getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(Address shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Set<OrderItem> getOrderItems() {
		return orderItems;
	}

//	public void setOrderItems(Set<OrderItem> orderItems) {
//		this.orderItems = orderItems;
//	}
	
	
	
	



}
