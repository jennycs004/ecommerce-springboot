package security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import beans.Customer;
import dao.CustomerDao;

@Service
public class CustomerServiceImpl implements UserDetailsService {

	@Autowired
	CustomerDao customerDao;
	
	
	// implements UserDetailsService allows us to pull data from database 
	// and use object UserDetails
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Customer customer = customerDao.findByUsername(username);
		if (customer == null) {
			throw new UsernameNotFoundException("Username" + username + "was not found");
		}
		
		return customer;
	}

//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
